"""Локаторы элементов на страницах"""
from selenium.webdriver.common.by import By



class VkLocators(object):
 
    LOGIN_INPUT = (By.XPATH, "//input[@id='index_email']")
    PASS_INPUT = (By.XPATH, "//input[@id='index_pass']")
    LOG_BUTTN = (By.XPATH, "//button[@id='index_login_button']")
    CHECK_BOX = (By.XPATH, "//div[@id='index_expire']")
    PHOTOS = (By.XPATH, "//div[@class='page_photos_module']/a[2]")
    HEART = (By.XPATH, "//div[@id='pv_like']")
    NEXTPHOTO = (By.XPATH, "//div[@class='pv_img_progress_wrap']")
    